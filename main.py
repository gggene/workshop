import pandas as pd
import matplotlib as plt
from sklearn import model_selection
from apyori import apriori
import argparse


def load_data(filename):
    data = pd.read_csv(filename)
    return data


#transform data into a list
# want to drop nan and find only the unique one
def preprocess(data):
    item_list = data.unstack().dropna().unique()
    print(item_list)
    print("the number of items =",len(item_list))

    #split data into 2 groups : train set and test set
    train,test = model_selection.train_test_split(data, test_size=.10)
    print("train = ",train)
    print("test = ",test)

    #transform data 
    # .T = transpost and drop NAN
    # .tolist = transform to list
    train = train.T.apply(lambda x : x.dropna().tolist()).tolist()
    print("------ Transform --------")
    #print(train)

    # make an input for the model
    print("-------------------------")
    #for i in train[:10]:
    #   print(i)

    return train,test

def model(train_data,min_support = 0.0045,min_confidence = 0.2, min_lift=3,min_length =2):
    result = list(apriori(train_data, min_support = min_support, 
    min_confidence = min_confidence, min_lift = min_lift, min_length=min_length))
    print("result = ")
    print(result)
    return result

def visualize(result):
    for rule in result:
        pair = rule[0]
        components = [i for i in pair]
        print(pair)
        print("Rule : ",components[0],"-->",components[1])
        print("Support : ",rule[1])
        print("Confidence : ",rule[2][0][2])
        print("Lift : ",rule[2][0][3])
        print("------------------------------------------")

def argument():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data", type=str , help = "csv file as a dataset")
    parser.add_argument("--min_length", type=int, default=3, help = "minimum number of product")
    return parser


if __name__ == "__main__" :
    parser = argument()
    args = parser.parse_args()
    print(args.data)
    data = load_data(args.data)
    print(data)
    train,test = preprocess(data)
    result = model(train, min_length=args.min_length)
    visualize(result)

